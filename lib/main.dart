import 'package:flare_flutter/flare_actor.dart';
import 'package:flare_flutter/flare_controls.dart';
import 'package:flutter/material.dart';

import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flare Controls',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flare Controls'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  // Store a reference to some controls for the Flare widget
  final FlareControls controls = FlareControls();

  AudioCache _audioCache;

  @override
  void initState() {
    super.initState();
    // create this only once
    _audioCache = AudioCache(prefix: "audio/", fixedPlayer: AudioPlayer()..setReleaseMode(ReleaseMode.STOP));
  }


  void _playWaveAnimation() {
    // Use the controls to trigger an animation.
    controls.play("Wave");
    _audioCache.play("bird.mp3");
  }

  void _playIdleAnimation() {
    // Use the controls to trigger an animation.
    controls.play("Idle");
  }

  void _playBlinkAnimation() {
    // Use the controls to trigger an animation.
    controls.play("Blink");
  }




  @override
  Widget build(BuildContext context) {

    return Scaffold(
      //backgroundColor: Colors.lightBlue,

      appBar: AppBar(
      ),

      body:
      Stack(
        alignment: Alignment.topCenter,
        children: <Widget>[

          //怪物flr头像
          Positioned(
            //width:  MediaQuery.of(context).size.width * .20,
            left: MediaQuery.of(context).size.width * .22,
            //       height: MediaQuery.of(context).size.height * 0.8,
            top: MediaQuery.of(context).size.height * 0.30,


            child: Container(
              height: 220,
              width: 220,
//              color: Colors.blue,
              child: FlareActor(
                "assets/Scanutt_ZhaYan.flr",
                animation: "Idle",
                fit: BoxFit.cover,
                //artboard: "artboard",
                //artboard: "Scene",
                  controller: controls,
              ),



            ),
          ),




          //三个按钮
          Positioned(
            //width:  MediaQuery.of(context).size.width * .20,
            left: MediaQuery.of(context).size.width * .08,
            //       height: MediaQuery.of(context).size.height * 0.8,
            top: MediaQuery.of(context).size.height * 0.70,


            child:Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[

                RaisedButton
                  (
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    //side: BorderSide(color: Colors.red)
                  ),
                  padding: const EdgeInsets.all(8.0),
                  textColor: Colors.black,
                  color: Colors.yellow,
                  onPressed: _playIdleAnimation,
                  child: new Text("Idle"),
                ),


                SizedBox(width: 20,),
                RaisedButton
                  (
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    //side: BorderSide(color: Colors.red)
                  ),
                  padding: const EdgeInsets.all(8.0),
                  textColor: Colors.black,
                  color: Colors.yellow,
                  onPressed: _playBlinkAnimation,

                  child: new Text("Blink"),
                ),

                SizedBox(width: 20,),
                RaisedButton
                  (
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    //side: BorderSide(color: Colors.red)
                  ),
                  padding: const EdgeInsets.all(8.0),
                  textColor: Colors.black,
                  color: Colors.yellow,
                  onPressed: _playWaveAnimation,

                  child: new Text("Wave"),
                ),




              ],
            ),

          ),

        ],
      ),

    );
  } //end of build


}